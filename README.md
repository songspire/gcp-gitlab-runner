# Gitlab runner on GCP

This is a simple terraform script to deploy a [gitlab runner](https://docs.gitlab.com/runner/) on GCP. The gitlab runner is configured to start compute instances using docker-machine. This allow to start new executors when there are pending jobs in the queue and to delete executors wehn they are idle after all jobs have been executed.

The setup uses the [autoscaling feature](https://docs.gitlab.com/runner/configuration/autoscale.html) of gitlab runner.

## Registering your runner

Before a runner can be deployed it has to be registered on a gitlab server. For this a runner has to be configured for your porject as described [here](https://gitlab.com/help/ci/runners/README). Then using the gitlab-ci token a gitlab-runner can be registered to obtain the gitlab runner token which is added to the gitlab runner configuration to authorize the runner to execute jobs for the project(s) the token was issued for. The easiest way to get a gitlab runner token is to run a gitlab-runner in docker e.g. on a local machine and note the token from the generated config as described [here](https://docs.gitlab.com/runner/register/#one-line-registration-command).

## Deploying the runner

Once the gital runner token (not the registration token) has been acquired all the variables in variables.tf should be configured. The `executor_image_url` is the image used for the executors. Executors are provisioned by docker-machine, so the image has to be supported by docker-machine. Following images have been tested:

- https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-minimal-1804-lts
- https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/family/debian-10

As part of the provisioning docker is installed if it doesn't exist. To reduce the time needed to provision an executor an image with docker pre-installed can be built and used in `executor_image_url`.