
# The instance running gitlab-runner as a container.
resource "google_compute_instance" "gitlab-runner" {
    #allow_stopping_for_update = true
    name         = "gitlab-runner"
    machine_type = "e2-small"
    zone         = var.zone

    tags = ["gitlab-runner"]

    boot_disk {
        initialize_params {
        image = "cos-cloud/cos-stable"
        }
    }

    network_interface {
        network    = var.network
        subnetwork = var.subnet

        access_config {
        # Ephemeral IP
        }
    }

    # The template defines the startup script (systemd unit) to start the 
    # gitlab-runner container on startup and also writes the config file for
    # the gitlab-runner to use docker-machine for autoscaling as described in
    # https://docs.gitlab.com/runner/configuration/autoscale.html.
    metadata = {
        user-data = templatefile(
            "cloud-config.yml.tmpl", 
            {
                gitlab_runner_token = var.gitlab_runner_token,
                gitlab_url = var.gitlab_url,
                executor_machine_type = var.executor_machine_type,
                executor_image_url = var.executor_image_url,
                concurrent_builds = var.concurrent_builds,
                idle_count = var.idle_count,
                idle_time = var.idle_time,
                max_builds = var.max_builds,
                project_id = var.project_id,
                zone = var.zone,
                network = var.network,
                subnet = var.subnet,
            }
        )
    }

    service_account {
        scopes = ["userinfo-email", "compute-rw", "storage-rw"]
    }
}

# gitlab-runner is configured to use docker-machine to provision executors. 
# docker-machine requires a firewall rule named "docker-machines".
resource "google_compute_firewall" "docker-machines" {
  name    = "docker-machines"
  network = var.network

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_tags = ["gitlab-runner"]
  target_tags = ["gitlab-runner"]
}