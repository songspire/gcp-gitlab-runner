variable "project_id" {
  type = string
}

variable "region" {
    type = string
}

variable "zone" {
    type = string
}

variable "network" {
    type    = string
    default = "default"
}

variable "subnet" {
    type    = string
    default = "default"
}

variable "gitlab_runner_token" {
    type  = string
}

variable "gitlab_url" {
    description = "The URL of the Gitlab server to register this runner with"
    type = string
    default = "https://gitlab.com/"
}

variable "executor_machine_type" {
    type = string
    default = "e2-small"
}

variable "executor_image_url" {
    type = string
    default = "https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-minimal-1804-lts"
}

variable "concurrent_builds" {
    type = number
    default = 4
}

variable "idle_count" {
    type = number
    default = 1
}

variable "idle_time" {
    type = number
    default = 60
}

variable "max_builds" {
    type = number
    default = 100
}

variable "additional_metadata" {
  type        = map
  description = "Additional metadata to attach to the instance"
  default     = {}
}